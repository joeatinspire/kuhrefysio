# Kuhrefysio

A small fysiotherapist site made with Angular 2 talking with a WordPress REST API backend. Also my very first Angular 2 which is why it has many flaws that can be carved into diamonds.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## License

Released under [creative commons non-commercial licence](https://creativecommons.org/licenses/by-nc/3.0/us/) has no title attribute.