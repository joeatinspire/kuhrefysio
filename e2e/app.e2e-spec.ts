import { KuhrefysioPage } from './app.po';

describe('kuhrefysio App', function() {
  let page: KuhrefysioPage;

  beforeEach(() => {
    page = new KuhrefysioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
