import { Component, OnInit, Input } from '@angular/core';

@Component({
  
  selector: 'about-page',
  templateUrl: 'about-page.component.html',
  styleUrls: ['about-page.component.scss']
})
export class AboutPageComponent implements OnInit {
  @Input() page: any;

  constructor() { }

  ngOnInit() {
  }

}
