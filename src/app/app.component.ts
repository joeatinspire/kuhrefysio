import { Component, OnInit } from '@angular/core';
import { WordpressService } from './wordpress.service';
import { MetaService } from 'ng2-meta';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit {

  settings: any;

  constructor(private wordpress: WordpressService, private metaService: MetaService) {

  }

  ngOnInit() {
    this.wordpress.getSettings()
      .then(resolve => {
        this.settings = resolve.json().acf;
      });
  }
}
