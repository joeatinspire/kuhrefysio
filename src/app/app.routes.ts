// - Routes instead of RouteConfig
// - RouterModule instead of provideRoutes
import { Routes, RouterModule } from '@angular/router';

import { FrontpageComponent } from './frontpage/frontpage.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { PricePageComponent } from './price-page/price-page.component';
import { TreatmentPageComponent } from './treatment-page/treatment-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { PageContainerComponent } from './page-container/page-container.component';
import { SingleTreatmentPageComponent } from './single-treatment-page/single-treatment-page.component';

const routes: Routes = [
  {
    path: '',
    component: PageContainerComponent,
  },
  {
    path: ':slug',
    component: PageContainerComponent
  },
  {
    path: 'behandlinger/:slug',
    component: SingleTreatmentPageComponent
  },
  {
    path: ':parent/:slug',
    component: PageContainerComponent
  },
];

// - Updated Export
export const routing = RouterModule.forRoot(routes);
