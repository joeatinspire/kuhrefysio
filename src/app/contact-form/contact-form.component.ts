import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { WordpressService } from '../wordpress.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Component({

  selector: 'contact-form',
  templateUrl: 'contact-form.component.html',
  styleUrls: ['contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  registerForm: FormGroup;
  @Input() settings: any;
  result: any;
  phone: string;
  success: boolean = false;
  error: string;

  constructor(
    private formBuilder: FormBuilder,
    private wordpress: WordpressService,
    private http: Http
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: [],
      message: ['', Validators.required]
    });

    this.phone = this.settings.phone.replace(/\s/g, "");
  }

  onSubmit(value) {
    let body = JSON.stringify(value);

    if(this.registerForm.status == 'VALID') {
      this.result = this.http.post('http://apikuhrefysio.dosh.dk/wp-json/kuhrefysio/v1/contact/', body)
                      .toPromise()
                      .then((e) => {
                        if(e.json().success) {
                          this.success = e.json().success;
                        } else {
                          this.error = e.json().error;
                        }
                      })
                      .catch(this.handleError);
    }
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Promise.reject(errMsg);
  }

}
