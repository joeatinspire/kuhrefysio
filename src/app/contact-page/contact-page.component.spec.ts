/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { ContactPageComponent } from './contact-page.component';

describe('Component: ContactPage', () => {
  it('should create an instance', () => {
    let component = new ContactPageComponent();
    expect(component).toBeTruthy();
  });
});
