import { Component, OnInit, Input } from '@angular/core';
import { WordpressService } from '../wordpress.service';

@Component({

  selector: 'contact-page',
  templateUrl: 'contact-page.component.html',
  styleUrls: ['contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  @Input() page: any;

  settings: any;

  constructor(private wordpress: WordpressService) {

  }

  ngOnInit() {
    this.wordpress.getSettings()
      .then(resolve => {
        this.settings = resolve.json().acf;
      });
  }

}
