/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { ContentPageComponent } from './content-page.component';

describe('Component: ContentPage', () => {
  it('should create an instance', () => {
    let component = new ContentPageComponent();
    expect(component).toBeTruthy();
  });
});
