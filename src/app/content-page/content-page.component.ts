import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WordpressService } from '../wordpress.service';

@Component({

  selector: 'content-page',
  templateUrl: 'content-page.component.html',
  styleUrls: ['content-page.component.scss']
})
export class ContentPageComponent implements OnInit {

  private sub: any;
  private slug: any;
  @Input() page: any = {
    acf: {

    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private wordpress: WordpressService
  ) { }

  ngOnInit() {

  }

}
