/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { CtaBtnComponent } from './cta-btn.component';

describe('Component: CtaBtn', () => {
  it('should create an instance', () => {
    let component = new CtaBtnComponent();
    expect(component).toBeTruthy();
  });
});
