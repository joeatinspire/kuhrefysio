import { Component, OnInit, Input } from '@angular/core';

@Component({
  
  selector: 'cta-image',
  templateUrl: 'cta-image.component.html',
  styleUrls: ['cta-image.component.scss']
})
export class CtaImageComponent implements OnInit {
  @Input() image: string;
  style: string;

  constructor() { }

  ngOnInit() {
    this.style = 'background-image: url('+this.image+')';
  }

}
