import { Component, OnInit, Input } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

@Component({

  selector: 'footer-maps',
  templateUrl: 'footer-maps.component.html',
  styleUrls: ['footer-maps.component.scss']
})
export class FooterMapsComponent implements OnInit {
  @Input() settings: any;
  lat: number;
  lng: number;
  zoom: number = 16;
  icon: string = "/assets/images/map-marker.png";
  map: any;


  main_color = '#2d313f';
  saturation_value= -20;
  brightness_value= 5;

  style = [
	{
		//don't show highways lables on the map
        featureType: 'road.highway',
        elementType: 'labels',
        stylers: [
            {visibility: "off"}
        ]
    },
	{
		//set highway colors/brightness/saturation
		featureType: "road.highway",
		elementType: "geometry.fill",
		stylers: [
			{ hue: this.main_color },
			{ visibility: "on" },
			{ lightness: this.brightness_value },
			{ saturation: this.saturation_value }
		]
	},

	// other elements style here
];

  constructor() {

  }

  ngOnInit() {
    this.lat = parseFloat(this.settings.googlemaps_lat);
    this.lng = parseFloat(this.settings.googlemaps_long);
  }

  mapLoaded(m) {
    this.map = m;
  }

  zoomOut() {
    this.map.setZoom(this.map.getZoom()-1);
  }

  zoomIn() {
    this.map.setZoom(this.map.getZoom()+1);
  }

}
