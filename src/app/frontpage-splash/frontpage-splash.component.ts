import { Component, OnInit, Input } from '@angular/core';

@Component({
  
  selector: 'frontpage-splash',
  templateUrl: 'frontpage-splash.component.html',
  styleUrls: ['frontpage-splash.component.scss']
})
export class FrontpageSplashComponent implements OnInit {
  @Input() phone: string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
