import { Directive, Output, EventEmitter } from '@angular/core';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

@Directive({
  selector: 'maps-controller'
})
export class GoogleMapsController{
  @Output() mapLoaded = new EventEmitter();

  constructor(private _wrapper: GoogleMapsAPIWrapper) {

    this._wrapper.getNativeMap().then((m) => {
      this.mapLoaded.emit(m);
    });
  }
}
