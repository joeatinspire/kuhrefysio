import { Component, OnInit, Input } from '@angular/core';

@Component({
  
  selector: 'image-grid',
  templateUrl: 'image-grid.component.html',
  styleUrls: ['image-grid.component.scss']
})
export class ImageGridComponent implements OnInit {
  @Input() images: any;

  constructor() { }

  ngOnInit() {
  }

}
