import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'image-no-grid',
  templateUrl: './image-no-grid.component.html',
  styleUrls: ['./image-no-grid.component.scss']
})
export class ImageNoGridComponent implements OnInit {
  @Input() image: string;

  constructor() { }

  ngOnInit() {
  }

}
