import { Component, OnInit } from '@angular/core';
import { WordpressService } from '../wordpress.service';

@Component({

  selector: 'main-navigation',
  templateUrl: 'main-navigation.component.html',
  styleUrls: ['main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {
  menu: any;
  mobileNavOpen: boolean = false;

  constructor(private wordpress: WordpressService) { }

  ngOnInit() {
    this.wordpress.getMenu('main_nav')
      .then(result => {
        this.menu = result;
      });
  }

}
