import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WordpressService } from '../wordpress.service';

@Component({

  selector: 'app-page-container',
  templateUrl: 'page-container.component.html',
  styleUrls: ['page-container.component.scss']
})
export class PageContainerComponent implements OnInit {
  private sub: any;
  private slug: any;
  private page: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private wordpress: WordpressService
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.slug = params['slug'] ? params['slug'] : 'forsiden';

       this.wordpress.getPageBySlug(this.slug)
         .then(result => {
           this.page = result;
         })
         .catch((err) => {
           console.log(err);
         });

    });
  }

}
