import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'text-section',
  templateUrl: 'text-section.component.html',
  styleUrls: ['text-section.component.scss']
})
export class TextSectionComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() textAlign: string = '';
  @Input() noTitle: string | boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.noTitle == "false") {
      this.noTitle = true;
    }
  }

}
