import { Component, OnInit, Input } from '@angular/core';

@Component({

  selector: 'treatment-list',
  templateUrl: 'treatment-list.component.html',
  styleUrls: ['treatment-list.component.scss']
})
export class TreatmentListComponent implements OnInit {
  @Input() treatments: any;

  constructor() { }

  ngOnInit() {

  }

}
