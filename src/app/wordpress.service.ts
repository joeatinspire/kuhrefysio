import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { MetaConfig, MetaService } from 'ng2-meta';

@Injectable()
export class WordpressService {
  public test: any;
  public settings: any;
  private baseUrl = 'http://apikuhrefysio.dosh.dk/wp-json/wp/v2';
  private rootUrl = 'http://apikuhrefysio.dosh.dk/wp-json';

  constructor(private http: Http, private metaService: MetaService) {

   }

  getSettings() {
    if (!this.settings) {
      this.settings = this.http.get(`${this.rootUrl}/acf/v2/options`).toPromise()
      return this.settings;
    } else {
      return this.settings;
    }
  }

  getPageBySlug(slug: string) {
    return this.http.get(`${this.baseUrl}/pages/?filter[name]=${slug}`)
      .toPromise()
      .then(response => {

        if(typeof response.json()[0].title !== 'undefined' && response.json()[0].title !== null) {
            this.metaService.setTitle(response.json()[0].title.rendered);
        }

        if(typeof response.json()[0].better_featured_image !== 'undefined' && response.json()[0].better_featured_image !== null) {
          this.metaService.setTag('og:image',response.json()[0].better_featured_image.source_url);
        }

        return response.json()[0];
      })
      .catch(err => {
        console.log(err);
        return {};
      });
  }

  getTreatmentBySlug(slug: string) {
    return this.http.get(`${this.baseUrl}/treatment/?filter[name]=${slug}`)
      .toPromise()
      .then(response => {

        if(typeof response.json()[0].title !== 'undefined' && response.json()[0].title !== null) {
            this.metaService.setTitle(response.json()[0].title.rendered);
        }

        if(typeof response.json()[0].better_featured_image !== 'undefined' && response.json()[0].better_featured_image !== null) {
          this.metaService.setTag('og:image',response.json()[0].better_featured_image.source_url);
        }

        return response.json()[0];
      })
      .catch(err => {
        console.log(err);
        return {};
      });
  }

  getMenu(location) {
    return this.http.get(`${this.rootUrl}/wp-api-menus/v2/menu-locations/${location}`)
      .toPromise()
      .then(response => {
        let result = response.json();
        for(let i = 0; i < result.length; i++) {
          result[i].url = result[i].url.replace('http://apikuhrefysio.dosh.dk', '');

          for(let u = 0; u < result[i].children.length; u++) {
            result[i].children[u].url = result[i].children[u].url.replace('http://apikuhrefysio.dosh.dk', '');
          }
        }
        return result;
      })
      .catch(err => {
        console.error(err);
      })
  }
}
